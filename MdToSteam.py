import glob

def ReplaceSubstringAlternating(FullText, SubString, Replacement1, Replacement2):
    while SubString in FullText:
        FullText = FullText.replace(SubString, Replacement1, 1)
        FullText = FullText.replace(SubString, Replacement2, 1)

    return FullText

def FileReplacements(FullText):
    NewTxT = ""
    # Horizontal rules work only when the whole line is those chars
    for CLine in FullText.splitlines():
        if CLine == "***" or CLine == "---" or CLine == "___":
            NewTxT += "[hr][/hr]\n"
        else:
            NewTxT += CLine + "\n"

    NewTxT = ReplaceSubstringAlternating(NewTxT, "__", "[b]", "[/b]")
    NewTxT = ReplaceSubstringAlternating(NewTxT, "**", "[b]", "[/b]")
    NewTxT = ReplaceSubstringAlternating(NewTxT, "_", "[i]", "[/i]")
    NewTxT = ReplaceSubstringAlternating(NewTxT, "~~", "[strike]", "[/strike]")
    # Preserve by temporarily turning into different special character
    NewTxT = NewTxT.replace("* ", "[|\|]")
    NewTxT = ReplaceSubstringAlternating(NewTxT, "*", "[i]", "[/i]")
    NewTxT = NewTxT.replace("[|\|]", "[*]")
    NewTxT = NewTxT.replace("<br>", "\n")
    return NewTxT


def HeaderReplacement(FullText):
    NewText = ""
    for CLine in FullText.splitlines():
        Index = 0
        if CLine == "":
            NewText += "\n"
            continue
        CurrentC = CLine[Index]
        if CurrentC == "#":
            while(CurrentC == "#"):
                Index += 1
                CurrentC = CLine[Index]

            # Only do the actual replacement if there is space after # chars
            CurrentC = CLine[Index]
            if CurrentC == " ":
                # Clamp to max steam supported header
                Header = "h" + str(min(Index, 3))
                CLine = "[" + Header + "]" + CLine[Index + 1 : len(CLine) - 1] + "[/" + Header + "]"
        NewText += CLine + "\n"
    return NewText


def LinkReplacement(FullText):
    NewText = ""
    for CLine in FullText.splitlines():
        LinkMid = CLine.find("](")
        while (LinkMid != -1):
            LinkStart = LinkMid - 1
            while LinkStart >= 0:
                if CLine[LinkStart] == "[":
                    break
                else:
                    LinkStart -= 1

            LinkEnd = CLine[LinkMid + 2 : len(CLine)].find(")") + LinkMid + 2
            Link = CLine[LinkMid + 2 : LinkEnd]
            LinkTxT = CLine[LinkStart + 1 : LinkMid]
            if (LinkEnd != -1 and LinkMid != -1 and LinkStart != -1):
                CLine = CLine[0 : LinkStart] + "[url=" + Link + "]" + LinkTxT + "[/url]" + CLine[LinkEnd + 1: len(CLine)]
            LinkMid = CLine.find("](")
        NewText += CLine + "\n"
    return NewText


def LineReplacement(FullText, SearchSubString, StartSubString, EndSubString, DeleteAfter = True):
    NewList = True
    NewText = ""
    for CLine in FullText.splitlines():
        Index = CLine.find(SearchSubString)
        # If only at the start of the line
        if Index != -1 and Index < len(SearchSubString):
            if NewList:
                NewText += StartSubString + "\n"
                NewList = False
            if DeleteAfter:
                CLine = CLine.replace(SearchSubString, "", 1)
        else:
            if not NewList:
                NewText += EndSubString + "\n"
                NewList = True

        NewText += CLine + "\n"
        
    # Take care when files end with a list
    if not NewList:
        NewText += EndSubString + "\n"

    return NewText


def OrderedListReplacement(FullText):
    NewList = True
    NewText = ""
    for CLine in FullText.splitlines():
        if CLine == "":
            NewText += "\n"
            continue
        
        NumberString = CLine[0]
        if NumberString.isdigit() and CLine[1] == ".":
            if NewList:
                NewText += "[olist]\n"
                NewList = False
            # Skip the markdown required space
            CLine = "[*]" + CLine[3:len(CLine)]
        else:
            if not NewList:
                NewText += "[/olist]\n"
                NewList = True
        NewText += CLine + "\n"
        
    # Take care when files end with a list
    if not NewList:
        NewText += "[/list]\n"
    return NewText


MdFiles = glob.glob('./*.md')
for x in MdFiles:
    f = open(x)
    TxTText = f.read()
    f.close()

    TxTText = FileReplacements(TxTText)
    TxTText = HeaderReplacement(TxTText)
    TxTText = LinkReplacement(TxTText)
    
    TxTText = LineReplacement(TxTText, "[*]", "[list]", "[/list]", False)
    TxTText = LineReplacement(TxTText, "    ", "[code]", "[/code]")
    TxTText = LineReplacement(TxTText, ">", "[quote]", "[/quote]")
    TxTText = OrderedListReplacement(TxTText)
            
    print(x + "\n")
    print(TxTText)
    print("\n")

    # Create the txt file and write the result
    Txt = open(x.replace(".md", ".txt"), "w")
    Txt.write(TxTText)
    Txt.close