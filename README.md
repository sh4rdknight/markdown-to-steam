Doesn't support [table], [noparse], [spoiler] from Steam formats options.

Simple as possible, just not the fastest Python program to convert [Markdown](https://www.markdownguide.org/basic-syntax/) into [Steam formatting](https://steamcommunity.com/comment/Recommendation/formattinghelp).<br>
Will convert all .md files from the directory where was executed and save results in .txt files with Steam formatting.

Additionally prints converted text into cmd/console if run from IDE.